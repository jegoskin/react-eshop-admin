import React, { useState, useEffect } from 'react';
import { Modal, Input, InputNumber, Select, Divider, Form } from 'antd';
import { inlineFormItem } from '../../lib/constants.js'
const Option = Select.Option;

const AddProductModal = ({ showModal, product, id, enums, quantity = [], onHide, onSubmit }) => {
  const [ newName, setNewName ] = useState('');
  const [ newBrandId, setNewBrandId ] = useState('');
  const [ newCategoryId, setNewCategoryId ] = useState('');
  const [ newPrice, setNewPrice ] = useState('');
  const [ newSize, setNewSize ] = useState({});

  console.log(product, quantity);

  useEffect(() => {
    if (showModal) {
      setNewName(product?.name);
      setNewPrice(product?.price);
      setNewBrandId(product?.brand._id);
      setNewCategoryId(product?.category._id);

      const quantityMap = new Map();
      quantity.forEach(item =>
        quantityMap.set(item.size, {...item})
      )

      setNewSize(() => {
        return enums.sizes.reduce((obj, size) => {
          obj[size._id]={
            quantity: quantityMap.size !== 0 ? quantityMap.get(size._id).quantity : 0,
            price: quantityMap.size !== 0 ? quantityMap.get(size._id).price : 0
          };
          return obj;
        }, {});
      })
    }
  }, [ showModal, product, enums, quantity ]);

  async function onOk() {
    await onSubmit(newName, newPrice, newBrandId, newCategoryId, newSize, id);
    setNewName('');
    setNewPrice('');
    setNewSize({});
    setNewBrandId('');
    setNewCategoryId('');
  }

  async function onCancel() {
    await onHide();
    setNewName('');
    setNewPrice('');
    setNewSize({});
    setNewBrandId('');
    setNewCategoryId('');
  }

  function handleChangeBrand(value) {
		setNewBrandId(value);
  }

  function handleChangeCategory(value) {
    setNewCategoryId(value)
  }

  function handleChangeSizePrice(e, { _id }) {
    const newItem = { ...newSize[_id] , price: parseFloat(e.target.value) || 0 };
    setNewSize({ ...newSize, [_id]: newItem })
  }

  function handleChangeSizeQuantity(e, { _id }) {
    const newItem = { ...newSize[_id] , quantity: parseFloat(e.target.value) || 0 };
    setNewSize({ ...newSize, [_id]: newItem })
  }

  return (
    <div>
      <Modal
        title = {`${id? 'Edit' : 'Add'} product`}
        onOk = {() => onOk()}
        onCancel = {() => onCancel()}
        visible = {showModal}
      >
        <Form
          onSubmit={() => onOk()}
          layout="horizontal"
          wrapperCol={
            {
              xs: {
                span: 24,
                offset: 0,
              },
              sm: {
                span: 16,
                offset: 8,
              }
            }
          }
        >
          <Form.Item label="Name" {...inlineFormItem}>
            <Input
              value = {newName}
              onChange = {e => setNewName(e.target.value)}
            />
          </Form.Item>
          <Form.Item label="Brand" {...inlineFormItem}>
            <Select
              style = {{marginBottom: '.5rem'}}
              onChange = {handleChangeBrand}
              value = {newBrandId}
            >
              { enums?.brands.map(item => <Option key={item._id} value={item._id.toString()}>{item.name}</Option>) }
            </Select>
          </Form.Item>
          <Form.Item label="Category" {...inlineFormItem}>
            <Select
              style = {{marginBottom: '.5rem'}}
              onChange = {handleChangeCategory}
              value = {newCategoryId}
            >
              { enums?.categories.map(item => <Option key={item._id} value={item._id.toString()}>{item.name}</Option>) }
            </Select>
          </Form.Item>
          <Form.Item label="Main price" {...inlineFormItem}>
            <InputNumber
              value={newPrice}
              onChange={value => setNewPrice(value)}
              style={{ marginBottom: '.5rem' }}
            />
          </Form.Item>
          <Divider />
          {
            enums?.sizes.map(item =>
              <div key={item._id}>
                <Form.Item label={`${item.name}`} {...inlineFormItem}>
                  <Input
                    value={newSize[item._id]?.price ?? ''}
                    onChange={value => handleChangeSizePrice(value, item)}
                  />
                </Form.Item>
                <Form.Item label='price' {...inlineFormItem}>
                  <Input
                    value={newSize[item._id]?.quantity}
                    onChange={value => handleChangeSizeQuantity(value, item)}
                  />
                </Form.Item>
              </div>
            )
          }
        </Form>
      </Modal>
    </div>
  )
}

export default AddProductModal;