import React, { useEffect, useState } from 'react';
import useSWR from 'swr';
import productApi from '../../lib/api/product.api';
import brandApi from '../../lib/api/brand.api';
import sizeApi from '../../lib/api/size.api';
import quantityApi from '../../lib/api/quantity';
import categoryApi from '../../lib/api/category.api';
import { Card, Button, Table, Input, Checkbox } from 'antd';
import AddProductModal from './add-product-modal';
import style from '../../styles/table.module.css';
import useFilter from '../../lib/filter.hook';
import { PlusCircleOutlined } from '@ant-design/icons';

function Product() {
  const [ modal, setModal ] = useState(false);

  const [ pageIndex, pageSize, filterFiled, filterString, sorter, filterQuery ] = useFilter(undefined);

  // const [ products, setProducts ] = useState([]);
  const { data: products } = useSWR(`/api/product?${filterQuery}`);

  const { data: brands } = useSWR(`/api/brand`);
  const { data: categories } = useSWR(`/api/category`);
  const { data: sizes } = useSWR(`/api/size`);


  // const [ pageIndex, setPageIndex ] = useState(undefined);
  // const [ pageSize, setPageSize ] = useState(undefined);
  // const [ filter, setFilter ] = useState({});
  // const [ sorter, setSorter ] = useState({ field: undefined, order: undefined });



  const [ loading, setLoading ] = useState(false);
  const [ editLoading, setEditLoading ] = useState(false);
  const [ selectedIndex, setSelectedIndex ] = useState(undefined);
  const [ selectedQuantity, setSelectedQuantity ] = useState(undefined);
  const [ enums, setEnums ] = useState({ brands: [], categories: [], sizes: [] });
  const [ pagination, setPagination ] = useState({ total: 0, current: 1, pageSize: null })


console.log('products', products);

  const columns = [
    {
      title: 'Name',
      key: 'name',
      dataIndex: 'name',
      sorter: true,
      filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => {
        const input = React.createRef();
        setImmediate(() => {
          if (input.current) {
            input.current.focus();
          }
        });
        return (
          <div className={style.searchForm}>
            <Input
              ref={input}
              placeholder="Search..."
              value={selectedKeys[0]}
              onChange={e => setSelectedKeys(e.target.value? [e.target.value] : [])}
              onPressEnter={confirm}
            />
            <Button type="primary" onClick={confirm}>Search</Button>
            <Button onClick={clearFilters}>Reset</Button>
          </div>
        )
      }
    },
    {
      title: 'Brand',
      key: 'brand',
      render: (_, row) => {
        return row.brand.name
      },
      sorter: true,
      filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => {
        const input = React.createRef();
        setImmediate(() => {
          if (input.current) {
            input.current.focus();
          }
        });
        return (
          <div className={style.searchForm}>
            <Checkbox.Group
              options={enums.brands.map(item => ({ label: item.name, value: item._id }))}
              onChange={value => setSelectedKeys(value ? [value] : [])}
              defaultValue={selectedKeys}
            />
            <Button type="primary" onClick={confirm}>Select</Button>
            <Button onClick={clearFilters}>Reset</Button>
          </div>
        )
      }
    },
    {
      title: 'Category',
      key: 'category',
      render: (_, row) => {
        return row.category.name
      },
    },
    {
      title: 'Price',
      key: 'price',
      dataIndex: 'price',
    },
    {
      render: (_, row, index) => {
        return <>
          <Button loading={editLoading} style={{marginRight: '.3rem'}} type="primary" onClick={() => handleOpenModal(index)}>Edit</Button>
          <Button onClick={() => remove(row._id)}>Delete</Button>
        </>
      }
    }
  ]

  // useEffect(() => {
  //   refresh();
  // }, []);

  // async function refresh(pagination, filter, sorter) {
  //   setLoading(true);
  //   if (Object.keys(sorter || {}).length < 3) {
  //     sorter = { field: "date", order: "descend" }
  //   }

  //   let pageIndex = 0;
  //   if (pagination?.current !== undefined) {
  //     pageIndex = pagination.current - 1;
  //   }
  //   const pageSize = pagination?.pageSize || 10;

  //   try {
  //     const productData = await Promise.all(
  //       [
  //         brandApi.get(),
  //         categoryApi.get(),
  //         sizeApi.get(),
  //         productApi.get(pageIndex, pageSize, filter, sorter)
  //       ]
  //     );
  //     setEnums(
  //       {
  //         brands: productData[0],
  //         categories: productData[1],
  //         sizes: productData[2]
  //       }
  //     );
  //     // setProducts(productData[3].data);
  //     setPagination(
  //       {
  //         total: productData[3].pagination.count,
  //         current: productData[3].pagination.pageIndex + 1,
  //         pageSize: productData[3].pagination.pageSize
  //       }
  //     );
  //   } catch(err) {
  //     console.error(err);
  //   } finally {
  //     setLoading(false);
  //   }
  // }

  const handleOpenModal = async (index) => {
    setEditLoading(true);
    try {
      const productQuantity = await quantityApi.getById(products[index]?._id);
      setSelectedQuantity(productQuantity);
      showModal(index);
    } catch(err) {
      console.error(err);
    } finally {
      setEditLoading(false);
    }
  }

  const showModal = async (index) => {
    setModal(!modal);
    setSelectedIndex(index);
  }

  async function remove(id) {
    setLoading(true);
    try {
      await productApi.remove(id);
      // refresh();
    } catch(err) {
      console.error(err);
    } finally {
      setLoading(false);
    }
  }

  async function onSubmit(newName, newPrice, newBrandId, newCategoryId, newSize, id) {
    setLoading(true);
    try {
      if (id) {
        await productApi.put(newName, newBrandId, newPrice, newCategoryId, newSize, id);
      } else {
        await productApi.post(newName, newBrandId, newPrice, newCategoryId, newSize);
      }
      showModal()
      // refresh()
    } catch(err) {
      console.error(err);
    } finally {
      setLoading(false);
    }
  }

	return (
		<div>
      <Card
        title={
          <div style={{display: 'flex', justifyContent: 'space-between', alignItems: 'center'}}>
            <h2 style={{margin: '0'}}>Product</h2><PlusCircleOutlined style={{ fontSize: '1.5rem', color: '#40a9ff' }} onClick={() => showModal()} />
          </div>
        }
      >
        <Table
          loading={loading}
          columns={columns}
          // dataSource={products}
          rowKey={row => row._id}
          // onChange={refresh}
          pagination={{
            current: pagination.current,
            pageSize: pagination.PageSize,
            pageSizeOptions: [ '10', '20', '50' ],
            showSizeChanger: true,
            total: pagination.total
          }}
        />
      </Card>

      <AddProductModal
        // product={products[selectedIndex]}
        // id={products[selectedIndex]?._id}
        quantity={selectedQuantity}
        enums={enums}
        showModal={modal}
        onHide={showModal}
        onSubmit={onSubmit}
      />
		</div>
	)
}

export default Product;