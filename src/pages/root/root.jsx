import React, { useContext } from 'react';
import Routing from './routing';
import { Layout, Button } from 'antd';
import Navigation from './navigation';
import { AuthContext } from '../../lib/context-api/auth-context';
import Login from '../root/login';
import { UserOutlined, LogoutOutlined } from '@ant-design/icons';

const { Header, Sider, Content } = Layout;

function Root() {
	const user = useContext(AuthContext);

	if (!user.isLogged) {
		return <Login/>
	}

	return (
		<div>
			<Layout style={{ minHeight: '100vh' }}>
				<Header style={{ display: 'flex', justifyContent: 'space-between', color: '#fff', padding: '0 1rem' }}>
					<h1 style={{ color: '#fff' }}>
						ADLER IPK
					</h1>
					<div>
						<UserOutlined /> {`${user.userInfo.firstName} ${user.userInfo.lastName}`} <Button icon={<LogoutOutlined />} type='link' onClick={() => user.logout()} />
					</div>
				</Header>
				<Layout>
					<Sider>
						<Navigation />
					</Sider>
					<Content style={{ padding: '.5rem' }}>
						<div style={{ padding: '1rem', backgroundColor: '#fff' }}>
							<Routing />
						</div>
					</Content>
				</Layout>
			</Layout>
		 	
		</div>
	);
}

export default Root;