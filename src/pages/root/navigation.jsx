import React from 'react';
import routes from '../../lib/routes';
import { Link } from 'react-router-dom';
import { Menu } from 'antd';
import { HomeOutlined, BookOutlined, DatabaseOutlined, TeamOutlined, SkinOutlined, AuditOutlined, SettingOutlined } from '@ant-design/icons';
const { SubMenu } = Menu;

function Navigation() {
  return (
    <div>
      <Menu
        theme="dark"
        mode="inline"
        defaultSelectedKeys={[window.location.href.split('/')[3] || '/']}
      >
        <Menu.Item key='/'>
          <Link to="/">
          <HomeOutlined /> Home
          </Link>
        </Menu.Item>
        <Menu.Item key={routes.POST} icon={<BookOutlined />}>
          <Link to={`/${routes.POST}`}>
            Post
          </Link>
        </Menu.Item>
        <SubMenu key="enums" icon={<DatabaseOutlined />} title={<span>Enums</span>}>
          <Menu.Item key={routes.BRAND}>
            <Link to={`/${routes.ENUM}/${routes.BRAND}`}>Brand</Link>
          </Menu.Item>
          <Menu.Item key={routes.CATEGORY}>
            <Link to={`/${routes.ENUM}/${routes.CATEGORY}`}>Category</Link>
          </Menu.Item>
          <Menu.Item key={routes.SIZE}>
            <Link to={`/${routes.ENUM}/${routes.SIZE}`}>Size</Link>
          </Menu.Item>
        </SubMenu>
        <Menu.Item key={routes.USER} icon={<TeamOutlined />}>
          <Link to={`/${routes.USER}`}>
            User
          </Link>
        </Menu.Item>
        <Menu.Item key={routes.PRODUCT} icon={<SkinOutlined />}>
          <Link to={`/${routes.PRODUCT}`}>
            Product
          </Link>
        </Menu.Item>
        <Menu.Item key={routes.ORDER} icon={<AuditOutlined />}>
          <Link to={`/${routes.ORDER}`}>
            Order
          </Link>
        </Menu.Item>
        <Menu.Item key={routes.SETTINGS} icon={<SettingOutlined />}>
          <Link to={`/${routes.SETTINGS}`}>
            Settings
          </Link>
        </Menu.Item>
      </Menu>
    </div>
  )
}

export default Navigation;