import React, { useState, useContext, useEffect } from 'react';
import { Form, Input, Button, Card, Col, Row, Spin } from 'antd';
import { AuthContext } from '../../lib/context-api/auth-context';
import { useHistory } from 'react-router-dom';
import { UserOutlined, LockOutlined } from '@ant-design/icons';

function LoginComponent() {
	const history = useHistory();
	const [ emailLogin, setEmailLogin ] = useState('');
	const [ passwordLogin, setPasswordLogin ] = useState('');
	const [ buttonLoading, setButtonLoading ] = useState(false);
	const [ pageLoading, setPageLoading ] = useState(true);
	const { login, refresh } = useContext(AuthContext);

	useEffect(() => {
		setPageLoading(true);
		try {
			refresh();
			setPageLoading(false);
		} catch(err) {
			console.error(err);
			setPageLoading(false);
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	async function handleLogin(e) {
		// e.preventDefault();

		console.log('object');

		try {
			setButtonLoading(true);
			await login(emailLogin, passwordLogin);
			history.push("/");
			setButtonLoading(false);
		} catch (error) {
			alert('Login Error');
			console.error(error);
			setButtonLoading(false);
		};
	}

	return (
		<div>
		{ pageLoading && <Spin size="large" /> }
		<Row type="flex" align="middle" style={{minHeight: '100vh'}}>
		<Col xs={{ span: 24 }} sm={{ span: 16, push: 4 }} md={{ span: 12, push: 6 }} lg={{ span: 8, push: 8 }} xl={{ span: 6, push: 9 }} >
			<Card title="Login">
				<Form onFinish={(e) => handleLogin(e)}>
					<Form.Item>
						<Input
							prefix={<UserOutlined style={{ color: 'rgba(0,0,0,.25)' }} />}
							value={emailLogin}
							placeholder="Username"
							onChange={(e) => setEmailLogin(e.target.value)}
						/>
					</Form.Item>
					<Form.Item>
						<Input
							prefix={ <LockOutlined style={{ color: 'rgba(0,0,0,.25)' }} />}
							type="password"
							placeholder="Password"
							value={passwordLogin}
							onChange={(e) => setPasswordLogin(e.target.value)}
						/>
					</Form.Item>
					<Form.Item>
						<Button type="primary" htmlType="submit" loading={buttonLoading}>
							Log in
						</Button>
					</Form.Item>
				</Form>
			</Card>
		</Col>
		</Row>
		</div>
	)
}

export default LoginComponent;