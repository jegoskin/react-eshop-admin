import React, { Component } from 'react';

class NotFound extends Component {
	render() {
		return (
			<div>
				<h1 style={{ fontSize: '80px', fontWeight: 'bold', textAlign: 'center' }}>404 - Not Found</h1>
			</div>
		)
	}
}

export default NotFound;