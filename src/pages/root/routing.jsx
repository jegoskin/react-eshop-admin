import React from 'react';
import { Route, Switch } from 'wouter';
import routes from '../../lib/routes';

import NotFound from './404';
import Home from '../home/home';
import Post from '../post/post';
import User from '../user/user';
import Brand from '../enum/brand/brand';
import Order from '../order/order';
import OrderDetail from '../order/order-detail';
import Category from '../enum/category/category';
import Size from '../enum/size/size';
import Product from '../product/product';
import Root_Login from './login';

function Routing() {
	return (
		<Switch>
			<Route path={`/${routes.LOGIN}`} exact component={Root_Login} />
			<Route path="/" exact component={Home} />
			<Route path={`/${routes.POST}`} exact component={Post} />
			<Route path={`/${routes.USER}`} exact component={User} />
			<Route path={`/${routes.PRODUCT}`} exact component={Product} />
			<Route path={`/${routes.ORDER}`} exact component={Order} />
			<Route path={`/${routes.ORDER}/:id`} exact component={({ params }) => <OrderDetail id={params.id} />} />
			<Route path={`/${routes.ENUM}/${routes.BRAND}`} exact component={Brand} />
			<Route path={`/${routes.ENUM}/${routes.CATEGORY}`} exact component={Category} />
			<Route path={`/${routes.ENUM}/${routes.SIZE}`} exact component={Size} />

			<Route component={NotFound} />
		</Switch>
	)
}

export default Routing;