import React, { useEffect, useState } from 'react';
import orderApi from '../../lib/api/order.api';
import { Card, Button, Table } from 'antd';
import AddOrder from './add-order-modal';
import { PlusCircleOutlined } from '@ant-design/icons';
import fetcher from '../../lib/fetcher';
import { useLocation } from 'wouter';

function Order() {
  const [ orders, setOrders ] = useState([]);
  const [ selectedOrder, setSelectedOrder ] = useState(undefined);
  const [ modal, setModal ] = useState(false);
  const [ location, setLocation ] = useLocation();

  const [ pagination, setPagination ] = useState({ total: 0, current: 1, pageSize: null })

  const columns = [
		{
      title: 'Id',
      key: '_id',
      dataIndex: '_id',
    },
    {
      title: 'Date',
      key: 'date',
      dataIndex: 'date',
    },
    {
      render: (_, row) => {
        return <>
          <Button style={{marginRight: '.3rem'}} type="primary" size="small" onClick={() => setLocation(`/order/${row._id}`)}>Edit</Button>
          <Button size="small" onClick={() => remove(row._id)}>Delete</Button>
        </>
      },
      width: '15%'
    }
  ]

  useEffect(() => {
    refresh();
  }, []);

  async function refresh(pagination, filter, sorter) {
		if (Object.keys(sorter || {}).length < 3) {
			sorter = { field: "date", order: "descend" }
    }

		let pageIndex = 0;
		if (pagination?.current !== undefined) {
			pageIndex = pagination.current - 1;
    }
    const pageSize = pagination?.pageSize || 10;

    try {
      const orderData = await orderApi.get(pageIndex, pageSize, filter, sorter);
      setOrders(orderData.data);
      setPagination(
        {
          total: orderData.pagination.count,
          current: orderData.pagination.pageIndex + 1,
          pageSize: orderData.pagination.pageSize
        }
      );
    } catch(err) {
      console.error(err);
    }
  }

  async function showEditModal(row) {
    try {
      const orderDetail = await fetcher(`/api/order/${row._id}`);
      setSelectedOrder(orderDetail);
      setModal(true);
    } catch(err) {
      console.error(err);
    }
  }

  async function onSubmit(data) {
    try {
      if (data._id) {
        console.log('PUT', data);
        await fetcher(`/api/order/${data._id}`, { method: 'PUT', body: data });
      } else {
        console.log('POST', data);
        await fetcher(`/api/order/`, { method: 'POST', body: data });
      }
      setModal(false)
      refresh();
    } catch(err) {
      console.error(err);
    }
  }

  async function remove(id) {
    try {
      await orderApi.remove(id);
      refresh();
    } catch(err) {
      console.error(err);
    }
  }

  function modalHide() {
    setModal(false);
    setSelectedOrder(undefined);
  }

	return (
		<div>
      <Card
        title={
          <div style={{display: 'flex', justifyContent: 'space-between', alignItems: 'center'}}>
            <h2 style={{margin: '0'}}>Orders</h2><PlusCircleOutlined style={{ fontSize: '1.5rem', color: '#40a9ff' }} onClick={() => setModal(true)} />
          </div>
        }
      >
        <Table
          columns={columns}
          dataSource={orders}
          rowKey={row => row._id}
          onChange={refresh}
          pagination = {{
            current: pagination.current,
            pageSize: pagination.PageSize,
            pageSizeOptions: ['10','20','50'],
            showSizeChanger: true,
            total: pagination.total
          }}
        />
      </Card>

      <AddOrder
        selectedOrder={selectedOrder}
        showModal={modal}
        onHide={modalHide}
        onSubmit={onSubmit}
      />
		</div>
	)
}

export default Order;