import React from 'react';
import { Input, Select, InputNumber } from 'antd';
import useSWR from 'swr';
import { MinusCircleOutlined } from '@ant-design/icons';
import styles from './order.module.css';
const Option = Select.Option;

function OrderItem({ index, item, removeProduct, updateProduct }) {
  const { data: sizes } = useSWR(`/api/size`);
  const { data: brands } = useSWR(`/api/brand`);

  console.log('item', item);

  return (
    <div className={styles.productCard} key={Date.now()}>
      <Select
        defaultValue={item.brandId}
        placeholder='Brand'
        onChange={(item) => updateProduct({ brandId: item }, index)}
        style={{width: '100%'}}
      >
        { brands?.map(item => <Option key={item._id} value={item._id}>{item.name}</Option>) }
      </Select>
      <Select
        defaultValue={item.productId}
        placeholder='Product'
        onChange={(item) => updateProduct({ productId: item }, index)}
        style={{width: '100%'}}
      >
        { brands?.map(item => <Option key={item._id} value={item._id}>{item.name}</Option>) }
      </Select>
      <Select
        defaultValue={item.sizeId}
        placeholder='Size'
        onChange={(item) => updateProduct({ sizeId: item }, index)}
        style={{width: '100%'}}
      >
        { sizes?.map(item => <Option key={item._id} value={item._id}>{item.name}</Option>) }
      </Select>
      <InputNumber placeholder='Count' value={item.count} onChange={(item) => updateProduct({ count: item }, index)} style={{width: '100%'}} />
      <Input disabled placeholder='Price'/>
      <MinusCircleOutlined style={{ fontSize: '1.5rem', color: '#CD5C5C' }} onClick={() => removeProduct(index)} />
    </div>
  )
}

export default OrderItem;