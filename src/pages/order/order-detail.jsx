import React, { useState, useEffect } from 'react';
import { Input, Switch, Select, Form, Divider, Button } from 'antd';
import useSWR from 'swr';
import styles from './order.module.css';
import fetcher from '../../lib/fetcher';
import { useLocation } from 'wouter';
const Option = Select.Option;

function OrderDetail({ id }) {
  const [ location, setLocation ] = useLocation();
  const { data: registeredUsers } = useSWR(`/api/user`);
  const { data } = useSWR(`/api/order/${id}`);
	const [ form ] = Form.useForm();
  const [ values, setValues ] = useState(undefined);

  useEffect(() => {
		if (data) {
      setValues(data);
      form.setFieldsValue(data);
    }
    //eslint-disable-next-line
  }, [ data ]);

  function selectUser(userId) {
    const selected = registeredUsers?.find(item => item._id === userId);
		form.setFieldsValue({ user: selected });
    setValues(prev => ({ ...prev, user: { ...selected, isRegistered: selected? true : false } }));
  }

  async function onSubmit() {
    console.log('submit');
    console.log('submit', id);
      try {
        if (id) {
          console.log('PUT', values);
          await fetcher(`/api/order/${id}`, { method: 'PUT', body: values });
        } else {
          console.log('POST', values);
          await fetcher(`/api/order/`, { method: 'POST', body: values });
        }
        form.resetFields();
      } catch(err) {
        console.error(err);
      }
  }

  return (
    <div>
        <Form
          form={form}
          id="form"
          onFinish={() => onSubmit()}
          onValuesChange={(_, values) => setValues(values)}
          labelCol={{ xl: 8, sm: 24 }}
        >

        <div style={{ display: 'grid', gridTemplateColumns: '1fr minmax(200px, 1fr) minmax(200px, 1fr)', gap: '1rem' }}>
          <div>
            <h3>Invoice address</h3>
            <Form.Item name={['user', 'firstName']} label="First name" className={styles.inputMain}>
              <Input />
            </Form.Item>
            <Form.Item name={['user', 'lastName']} label="Last name" className={styles.inputMain}>
              <Input />
            </Form.Item>
            <Form.Item name={['user', 'invoiceCompanyName']} label="Company name" className={styles.inputMain}>
              <Input />
            </Form.Item>
            <Form.Item name={['user', 'invoiceCompanyCin']} label="CIN" className={styles.inputMain}>
              <Input />
            </Form.Item>
            <Form.Item name={['user', 'invoiceCompanyVatin']} label="VATIN" className={styles.inputMain}>
              <Input />
            </Form.Item>
            <Form.Item name={['user', 'invoiceStreet']} label="Street" className={styles.inputMain}>
              <Input />
            </Form.Item>
            <Form.Item name={['user', 'invoiceZipCode']} label="Zip code" className={styles.inputMain}>
              <Input />
            </Form.Item>
            <Form.Item name={['user', 'invoiceTown']} label="Town" className={styles.inputMain}>
              <Input />
            </Form.Item>
            <Form.Item name={['user', 'invoiceCountry']} label="Country" className={styles.inputMain}>
              <Input />
            </Form.Item>
          </div>
          <div>
            <h3>Shipping address</h3>
            <Form.Item name={['user', 'shippingFirstName']} label="First name" className={styles.inputMain}>
              <Input />
            </Form.Item>
            <Form.Item name={['user', 'shippingLastName']} label="Last name" className={styles.inputMain}>
              <Input />
            </Form.Item>
            <Form.Item name={['user', 'shippingCompanyName']} label="Company name" className={styles.inputMain}>
              <Input />
            </Form.Item>
            <Form.Item name={['user', 'shippingStreet']} label="Street" className={styles.inputMain}>
              <Input />
            </Form.Item>
            <Form.Item name={['user', 'shippingZipCode']} label="Zip code" className={styles.inputMain}>
              <Input />
            </Form.Item>
            <Form.Item name={['user', 'shippingTown']} label="Town" className={styles.inputMain}>
              <Input />
            </Form.Item>
            <Form.Item name={['user', 'shippingCountry']} label="Country" className={styles.inputMain}>
              <Input />
            </Form.Item>
          </div>
          <div>
            <h3>Additional</h3>
            <Form.Item label="Is registered" name={['user', 'isRegistered']} className={styles.inputMain}>
              <Switch checked={values?.user?.isRegistered} />
            </Form.Item>
            <Form.Item label="Selected user" className={styles.inputMain}>
              <Select
                disabled={!values?.user?.isRegistered}
                onChange={selectUser}
                value={values?.user?._id}
              >
                { registeredUsers?.map(item => <Option key={item._id} value={item._id.toString()}>{ `${item.lastName} ${item.firstName}` }</Option>) }
              </Select>
            </Form.Item>
            <Form.Item name={['user', 'email']} label="Email" className={styles.inputMain}>
              <Input />
            </Form.Item>
            <Form.Item name={['user', 'telephone']} label="Telephone" className={styles.inputMain}>
              <Input />
            </Form.Item>
          </div>
        </div>
        <Divider/>
        <div className={styles.detailButton}>
          <Button type='primary' htmlType="button" onClick={() => setLocation('/order')}>Cancel</Button>
          <Button type='primary' htmlType="button" onClick={onSubmit}>Save</Button>
        </div>
        </Form>
    </div>
  )
}

export default OrderDetail;