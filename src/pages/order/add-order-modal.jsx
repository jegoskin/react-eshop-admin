import React, { useState } from 'react';
import { Modal, Input, Switch, Select, Form, Card } from 'antd';
import useSWR from 'swr';
import { PlusCircleOutlined } from '@ant-design/icons';
import styles from './order.module.css';
import OrderItem from './order-item';
const Option = Select.Option;

const AddOrderModal = ({ showModal, onHide, onSubmit }) => {
  const { data: registeredUsers } = useSWR(`/api/user`);
	const [ form ] = Form.useForm();
  const [ values, setValues ] = useState(undefined);
  const [ products, setProducts ] = useState([]);

  function selectUser(userId) {
    const selected = registeredUsers?.find(item => item._id === userId);
		form.setFieldsValue({ user: selected });
    setValues(prev => ({ ...prev, user: { ...selected, isRegistered: selected? true : false } }));
  }

  console.log('products', products);

  async function onOk() {
    await onSubmit(values);
    setValues(undefined);
    form.resetFields();
  }

  async function onCancel() {
    await onHide();
    setValues(undefined);
    form.resetFields();
  }

  function addProduct() {
    setProducts(prev => [...prev, { brandId: undefined, productId: undefined, sizeId: undefined, count: 0, price: 0 }]);
  }

  function removeProduct(index) {
    let newProducts = [ ...products ];
    newProducts.splice(index, 1)
    setProducts(newProducts);
  }

  function updateProduct(newObject, index) {
    let newProducts = [ ...products ];
    newProducts.splice(index,1, { ...products[index], ...newObject })
    setProducts(newProducts);
  }

  return (
    <div>
      <Modal
        title='New order'
        okButtonProps={{ htmlType: 'submit', form: 'form' }}
        onCancel={onCancel}
        visible={showModal}
        width='80%'
      >
        <Form
          form={form}
          id="form"
          onFinish={onOk}
          onValuesChange={(_, values) => setValues(prev => ({ ...prev, ...values }))}
          labelCol={{ xl: 8, sm: 24 }}
        >
        <div style={{ display: 'grid', gridTemplateColumns: '1fr minmax(200px, 1fr) minmax(200px, 1fr)', gap: '1rem' }}>
          <div>
            <h3>Invoice address</h3>
            <Form.Item name={['user', 'firstName']} label="First name" className={styles.inputMain}>
              <Input />
            </Form.Item>
            <Form.Item name={['user', 'lastName']} label="Last name" className={styles.inputMain}>
              <Input />
            </Form.Item>
            <Form.Item name={['user', 'invoiceCompanyName']} label="Company name" className={styles.inputMain}>
              <Input />
            </Form.Item>
            <Form.Item name={['user', 'invoiceCompanyCin']} label="CIN" className={styles.inputMain}>
              <Input />
            </Form.Item>
            <Form.Item name={['user', 'invoiceCompanyVatin']} label="VATIN" className={styles.inputMain}>
              <Input />
            </Form.Item>
            <Form.Item name={['user', 'invoiceStreet']} label="Street" className={styles.inputMain}>
              <Input />
            </Form.Item>
            <Form.Item name={['user', 'invoiceZipCode']} label="Zip code" className={styles.inputMain}>
              <Input />
            </Form.Item>
            <Form.Item name={['user', 'invoiceTown']} label="Town" className={styles.inputMain}>
              <Input />
            </Form.Item>
            <Form.Item name={['user', 'invoiceCountry']} label="Country" className={styles.inputMain}>
              <Input />
            </Form.Item>
          </div>
          <div>
            <h3>Shipping address</h3>
            <Form.Item name={['user', 'shippingFirstName']} label="First name" className={styles.inputMain}>
              <Input />
            </Form.Item>
            <Form.Item name={['user', 'shippingLastName']} label="Last name" className={styles.inputMain}>
              <Input />
            </Form.Item>
            <Form.Item name={['user', 'shippingCompanyName']} label="Company name" className={styles.inputMain}>
              <Input />
            </Form.Item>
            <Form.Item name={['user', 'shippingStreet']} label="Street" className={styles.inputMain}>
              <Input />
            </Form.Item>
            <Form.Item name={['user', 'shippingZipCode']} label="Zip code" className={styles.inputMain}>
              <Input />
            </Form.Item>
            <Form.Item name={['user', 'shippingTown']} label="Town" className={styles.inputMain}>
              <Input />
            </Form.Item>
            <Form.Item name={['user', 'shippingCountry']} label="Country" className={styles.inputMain}>
              <Input />
            </Form.Item>
          </div>
          <div>
            <h3>Additional</h3>
            <Form.Item label="Is registered" name={['user', 'isRegistered']} className={styles.inputMain}>
              <Switch checked={values?.user?.isRegistered} />
            </Form.Item>
            <Form.Item label="Selected user" className={styles.inputMain}>
              <Select
                disabled={!values?.user?.isRegistered}
                onChange={selectUser}
                value={values?.user?._id}
              >
                { registeredUsers?.map(item => <Option key={item._id} value={item._id.toString()}>{ `${item.lastName} ${item.firstName}` }</Option>) }
              </Select>
            </Form.Item>
            <Form.Item name={['user', 'email']} label="Email" className={styles.inputMain}>
              <Input />
            </Form.Item>
            <Form.Item name={['user', 'telephone']} label="Telephone" className={styles.inputMain}>
              <Input />
            </Form.Item>
          </div>
        </div>
        <div>
          <Card size='small' title='Products' extra={<PlusCircleOutlined style={{ fontSize: '1.5rem', color: '#40a9ff' }} onClick={() => addProduct()} />}>
            {
              products.map((item, index) =>
                <OrderItem
                  key={index}
                  item={item}
                  index={index}
                  removeProduct={removeProduct}
                  updateProduct={updateProduct}
                />
              )
            }
          </Card>
        </div>
        </Form>
      </Modal>
    </div>
  )
}

export default AddOrderModal;