import React, { useState, useEffect } from 'react';
import { Modal, Input, Select, Form, Divider, Button } from 'antd';

const { Option } = Select;

const AddBrandModal = ({ showModal, selectedUser, onHide, onSubmit, onSubmitPassword, roles }) => {
	const [ form ] = Form.useForm();
  const [ userData, setUserData ] = useState(undefined);

  useEffect(() => {
    if (showModal) {
			form.setFieldsValue({ ...selectedUser, password: undefined });
      setUserData(selectedUser);
    }
  }, [ showModal, selectedUser, form ]);

  async function onOk() {
    await onSubmit(userData);
  }

  async function onCancel() {
    await onHide();
  }

  async function savePassword() {
    await onSubmitPassword(userData);
  }

  return (
    <div>
      <Modal
        title={`${userData?._id? 'Edit' : 'Add'} user`}
        okButtonProps={{ htmlType: 'submit', form: 'form' }}
        // onOk={() => onOk()}
        onCancel={() => onCancel()}
        visible={showModal}
        width='800px'
      >

        <Form
          form={form}
          id="form"
          onFinish={onOk}
          onValuesChange={(_, values) => setUserData(prev => ({ ...values, _id: prev._id }))}
          labelCol={{ xl: 8, sm: 24 }}
        >

        <div style={{ display: 'grid', gridTemplateColumns: '1fr minmax(200px, 1fr)', gap: '1rem' }}>
          <div>
            <Form.Item name="firstName" label="First Name">
              <Input/>
            </Form.Item>
            <Form.Item name="telephone" label="Telephone">
              <Input/>
            </Form.Item>
            <Form.Item name="role" label="Role">
              <Select>
                { roles?.map((item, index) => <Option key={index} value={item.name}>{item.name}</Option>) }
              </Select>
            </Form.Item>
          </div>
          <div>
            <Form.Item name="lastName" label="Last Name">
              <Input/>
            </Form.Item>
            <Form.Item name="email" label="Email">
              <Input/>
            </Form.Item>
          </div>
        </div>

          <Divider/>
          <div style={{ display: 'grid', gridTemplateColumns: '1fr minmax(200px, 1fr)', gap: '1rem' }}>
            <div>
              <Form.Item name="password" label="Password">
                <Input/>
              </Form.Item>
            </div>
            <div>
            {
              userData?._id &&
              <>
                <Button onClick={() => savePassword()}>Save</Button>
              </>
            }
            </div>
          </div>

          <Divider/>

          <div style={{ display: 'grid', gridTemplateColumns: '1fr minmax(200px, 1fr)', gap: '1rem' }}>
          <div>
            <h3>Invoice address</h3>
            <Form.Item name="invoiceCompanyName" label="Company name">
              <Input />
            </Form.Item>
            <Form.Item name="invoiceCompanyCin" label="CIN">
              <Input />
            </Form.Item>
            <Form.Item name="invoiceCompanyVatin" label="VATIN">
              <Input />
            </Form.Item>
            <Form.Item name="invoiceStreet" label="Street">
              <Input />
            </Form.Item>
            <Form.Item name="invoiceZipCode" label="Zip code">
              <Input />
            </Form.Item>
            <Form.Item name="invoiceTown" label="Town">
              <Input />
            </Form.Item>
            <Form.Item name="invoiceCountry" label="Country">
              <Input />
            </Form.Item>
          </div>
          <div>
            <h3>Shipping address</h3>
            <Form.Item name="shippingFirstName" label="First name">
              <Input />
            </Form.Item>
            <Form.Item name="shippingLastName" label="Last name">
              <Input />
            </Form.Item>
            <Form.Item name="shippingCompanyName" label="Company name">
              <Input />
            </Form.Item>
            <Form.Item name="shippingStreet" label="Street">
              <Input />
            </Form.Item>
            <Form.Item name="shippingZipCode" label="Zip code">
              <Input />
            </Form.Item>
            <Form.Item name="shippingTown" label="Town">
              <Input />
            </Form.Item>
            <Form.Item name="shippingCountry" label="Country" >
              <Input />
            </Form.Item>
          </div>
        </div>

          {
            userData?._id &&
            <>
            <Divider/>
            <div style={{ display: 'grid', gridTemplateColumns: '1fr minmax(200px, 1fr)', gap: '1rem' }}>
              <div>
                <Form.Item name="createDate" label="Create">
                  <Input disabled />
                </Form.Item>
              </div>
              <div>
                <Form.Item name="modifyDate" label="Modify">
                  <Input disabled />
                </Form.Item>
              </div>
            </div>
            </>
          }

        </Form>
      </Modal>
    </div>
  )
}

export default AddBrandModal;