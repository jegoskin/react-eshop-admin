import React, { useEffect, useState } from 'react';
import userApi from '../../lib/api/user.api';
import roleApi from '../../lib/api/role.api';
import { Card, Table, Input, Button, message } from 'antd';
import style from '../../styles/table.module.css';
import AddUserModal from './add-user-modal';
import { PlusCircleOutlined } from '@ant-design/icons';

function User() {
  const [ users, setUsers ] = useState([]);
  const [ roles, setRoles ] = useState([]);
  const [ modal, setModal ] = useState(false);
  const [ selectedIndex, setSelectedIndex ] = useState(undefined);

  const [ paginationTotal, setPaginationTotal ] = useState(0);
  const [ paginationCurrent, setPaginationCurrent ] = useState(1);
  const [ paginationPageSize, setPaginationPageSize ] = useState(10);

  useEffect(() => {
    refresh();
  }, []);

  const columns = [
    {
      title: 'First Name',
      key: 'firstName',
      dataIndex: 'firstName',
      sorter: true,
      filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => {
        const input = React.createRef();
        setImmediate(() => {
          if (input.current) {
            input.current.focus();
          }
        });
        return (
          <div className={style.searchForm}>
            <Input
              ref={input}
              placeholder="Search..."
              value={selectedKeys[0]}
              onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
              onPressEnter={confirm}
            />
            <Button type="primary" onClick={confirm}>Search</Button>
            <Button onClick={clearFilters}>Reset</Button>
          </div>
        )
      }
    },
    {
      title: 'Last Name',
      key: 'lastName',
      dataIndex: 'lastName',
      sorter: true,
      filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => {
        const input = React.createRef();
        setImmediate(() => {
          if (input.current) {
            input.current.focus();
          }
        });
        return (
          <div className={style.searchForm}>
            <Input
              ref={input}
              placeholder="Search..."
              value={selectedKeys[0]}
              onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
              onPressEnter={confirm}
            />
            <Button type="primary" onClick={confirm}>Search</Button>
            <Button onClick={clearFilters}>Reset</Button>
          </div>
        )
      }
    },
    {
      title: 'Email',
      key: 'email',
      dataIndex: 'email'
    },
    {
      render: (_, row, index) => {
        return <>
          <Button style={{marginRight: '.3rem'}} type="primary" size="small" onClick={() => showModal(index)}>Edit</Button>
          <Button size="small" onClick={() => remove(row._id)}>Delete</Button>
        </>
      },
      width: '15%'
    }
  ]

  async function refresh(pagination, filter, sorter) {
    if (Object.keys(sorter || {}).length < 3) {
      sorter = { field: "date", order: "descend" }
    }

    let pageIndex = 0;
    if (pagination?.current !== undefined) {
      pageIndex = pagination.current - 1;
    }
    const pageSize = pagination?.pageSize || 10;

    try {
      const usersData = await userApi.get(pageIndex, pageSize, filter, sorter);
      setUsers(usersData.data);
      setPaginationTotal(usersData.pagination.count);
      setPaginationCurrent(usersData.pagination.pageIndex + 1);
      setPaginationPageSize(usersData.pagination.pageSize);
    } catch(err) {
      console.error(err);
    }
  }

  const showModal = async (index) => {
    try {
      const rolesData = await roleApi.get();
      setRoles(rolesData);
    } catch(err) {
      console.error(err);
    }
    setModal(!modal);
    setSelectedIndex(index);
  }

  async function remove(id) {
    try {
      await userApi.remove(id);
      refresh();
    } catch(err) {
      console.error(err);
    }
  }

  async function onSubmit(data) {
    console.log('submit data ', data);
    try {
      if (data._id) {
        await userApi.put(data);
      } else {
        await userApi.post(data);
      }
      showModal();
      refresh();
      message.success('Saved')
    } catch(err) {
      message.error('Error');
      console.error(err);
    }
  }

  async function onSubmitPassword(data) {
    console.log('submit data ', data);
    try {
      await userApi.postPassword(data);
      message.success('Saved');
    } catch(err) {
      message.error('Error');
      console.error(err);
    }
  }

  return (
    <div>
      <Card
        title={
          <div style={{display: 'flex', justifyContent: 'space-between', alignItems: 'center'}}>
            <h2 style={{margin: '0'}}>Users</h2>
            <PlusCircleOutlined style={{ fontSize: '1.5rem', color: '#40a9ff' }} onClick={() => showModal()} />
          </div>
        }
      >
        <Table
          columns={columns}
          dataSource={users}
          rowKey={row => row._id}
          pagination = {{
            current: paginationCurrent,
            pageSize: paginationPageSize,
            pageSizeOptions: [ '10', '20', '50' ],
            showSizeChanger: true,
            total: paginationTotal
          }}
          onChange={refresh}
        />
      </Card>

      <AddUserModal
        selectedUser = {users[selectedIndex]}
        showModal = {modal}
        roles = {roles}
        onHide = {showModal}
        onSubmit = {onSubmit}
        onSubmitPassword= {onSubmitPassword}
      />
		</div>
	)
}

export default User;