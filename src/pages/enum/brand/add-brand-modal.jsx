import React, { useState, useEffect } from 'react';
import { Modal, Input } from 'antd';

const AddBrandModal = ({showModal, name, id, onHide, onSubmit}) => {
  const [ newName, setNewName ] = useState(name);

  useEffect(() => {
    if (showModal) {
      setNewName(name);
    }
  }, [ showModal, name ]);

  async function onOk() {
    setNewName('');
    await onSubmit(newName, id);
  }

  async function onCancel() {
    setNewName('');
    await onHide();
  }

  return (
    <div>
      <Modal
        title={`${id? 'Edit' : 'Add'} brand`} 
        onOk={() => onOk()}
        onCancel={() => onCancel()}
        visible={showModal}
      >
        <Input 
          placeholder="Name" 
          value={newName} 
          onChange={e => setNewName(e.target.value)}
          style={{marginBottom: '1rem'}}
        />
      </Modal>
    </div>
  )
}

export default AddBrandModal;