import React, { useEffect, useState } from 'react';
import brandApi from '../../../lib/api/brand.api';
import { Card, Button, Table } from 'antd';
import AddBrandModal from './add-brand-modal';
import { PlusCircleOutlined } from '@ant-design/icons';

function Brand() {
  const [ brands, setBrands ] = useState([]);
  const [ modal, setModal ] = useState(false);
  const [ selectedIndex, setSelectedIndex ] = useState(undefined);

  const columns = [
    {
      title: 'Name',
      key: 'name',
      dataIndex: 'name',
    },
    {
      render: (_, row, index) => {
        return <>
          <Button style={{marginRight: '.3rem'}} type="primary" size="small" onClick={() => showModal(index)}>Edit</Button>
          <Button size="small" onClick={() => remove(row._id)}>Delete</Button>
        </>
      },
      width: '15%'
    }
  ]

	useEffect(() => {
    refresh();
  }, []);

  async function refresh() {
    try {
      const brands = await brandApi.get();
      setBrands(brands);
    } catch(err) {
      console.error(err);
    }
  }

  const showModal = (index) => {
    setModal(!modal);
    setSelectedIndex(index);
  }

  async function remove(id) {
    try {
      await brandApi.remove(id);
      refresh();
    } catch(err) {
      console.error(err);
    }
  }

  async function onSubmit(newName, id) {
    try {
      if (id) {
        await brandApi.put(newName, id);
      } else {
        await brandApi.post(newName);
      }
      showModal()
      refresh();
    } catch(err) {
      console.error(err);
    }
  }

	return (
		<div>
      <Card
        title={
          <div style={{display: 'flex', justifyContent: 'space-between', alignItems: 'center'}}>
            <h2 style={{margin: '0'}}>Brand</h2><PlusCircleOutlined style={{ fontSize: '1.5rem', color: '#40a9ff' }} onClick={() => showModal()} />
          </div>
        }
      >
        <Table
          columns={columns}
          dataSource={brands}
          rowKey={row => row._id}
          pagination={false}
          onChange={refresh}
        />
      </Card>

      <AddBrandModal
        name = {brands[selectedIndex]?.name}
        id = {brands[selectedIndex]?._id}
        showModal = {modal}
        onHide = {showModal}
        onSubmit = {onSubmit}
      />
		</div>
	)
}

export default Brand;