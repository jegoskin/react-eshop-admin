import React, { useEffect, useState } from 'react';
import sizeApi from '../../../lib/api/size.api';
import { Card, Button, Table } from 'antd';
import AddSizeModal from './add-size-modal';
import { PlusCircleOutlined } from '@ant-design/icons';

function Size() {
  const [ sizes, setSizes ] = useState([]);
  const [ modal, setModal ] = useState(false);
  const [ selectedIndex, setSelectedIndex ] = useState(undefined);

  const columns = [
    {
      title: 'Name',
      key: 'name',
      dataIndex: 'name',
    },
    {
      render: (_, row, index) => {
        return <>
          <Button style={{marginRight: '.3rem'}} type="primary" size="small" onClick={() => showModal(index)}>Edit</Button>
          <Button size="small" onClick={() => remove(row._id)}>Delete</Button>
        </>
      },
      width: '15%'
    }
  ]

	useEffect(() => {
    refresh();
  }, []);

  async function refresh() {
    try {
      const sizes = await sizeApi.get();
      setSizes(sizes);
    } catch(err) {
      console.error(err);
    }
  }

  const showModal = (index) => {
    setModal(!modal);
    setSelectedIndex(index);
  }

  async function remove(id) {
    try {
      await sizeApi.remove(id);
      refresh();
    } catch(err) {
      console.error(err);
    }
  }

  async function onSubmit(newName, id) {
    try {
      if (id) {
        await sizeApi.put(newName, id);
      } else {
        await sizeApi.post(newName);
      }
      showModal()
      refresh();
    } catch(err) {
      console.error(err);
    }
  }

	return (
		<div>
      <Card
        title={
          <div style={{display: 'flex', justifyContent: 'space-between', alignItems: 'center'}}>
            <h2 style={{margin: '0'}}>Size</h2><PlusCircleOutlined style={{ fontSize: '1.5rem', color: '#40a9ff' }} onClick={() => showModal()} />
          </div>
        }
      >
        <Table
          columns={columns}
          dataSource={sizes}
          rowKey={row => row._id}
          pagination={false}
          onChange={refresh}
        />
      </Card>

      <AddSizeModal
        name = {sizes[selectedIndex]?.name}
        id = {sizes[selectedIndex]?._id}
        showModal = {modal}
        onHide = {showModal}
        onSubmit = {onSubmit}
      />
		</div>
	)
}

export default Size;