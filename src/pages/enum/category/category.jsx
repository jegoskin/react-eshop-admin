import React, { useEffect, useState } from 'react';
import categoryApi from '../../../lib/api/category.api';
import { Card, Button, Table } from 'antd';
import AddCategoryModal from './add-category-modal';
import { PlusCircleOutlined } from '@ant-design/icons';

function Category() {
  const [ categories, setCategories ] = useState([]);
  const [ modal, setModal ] = useState(false);
  const [ selectedIndex, setSelectedIndex ] = useState(undefined);

  const columns = [
    {
      title: 'Name',
      key: 'name',
      dataIndex: 'name',
    },
    {
      render: (_, row, index) => {
        return <>
          <Button style={{marginRight: '.3rem'}} type="primary" size="small" onClick={() => showModal(index)}>Edit</Button>
          <Button size="small" onClick={() => remove(row._id)}>Delete</Button>
        </>
      },
      width: '15%'
    }
  ]

	useEffect(() => {
    refresh();
  }, []);

  async function refresh() {
    try {
      const categories = await categoryApi.get();
      setCategories(categories);
    } catch(err) {
      console.error(err);
    }
  }

  const showModal = (index) => {
    setModal(!modal);
    setSelectedIndex(index);
  }

  async function remove(id) {
    try {
      await categoryApi.remove(id);
      refresh();
    } catch(err) {
      console.error(err);
    }
  }

  async function onSubmit(newName, id) {
    try {
      if (id) {
        await categoryApi.put(newName, id);
      } else {
        await categoryApi.post(newName);
      }
      showModal()
      refresh();
    } catch(err) {
      console.error(err);
    }
  }

	return (
		<div>
      <Card
        title={
          <div style={{display: 'flex', justifyContent: 'space-between', alignItems: 'center'}}>
            <h2 style={{margin: '0'}}>Category</h2><PlusCircleOutlined style={{ fontSize: '1.5rem', color: '#40a9ff' }} onClick={() => showModal()} />
          </div>
        }
      >
        <Table
          columns={columns}
          dataSource={categories}
          rowKey={row => row._id}
          pagination={false}
          onChange={refresh}
        />
      </Card>

      <AddCategoryModal
        name = {categories[selectedIndex]?.name}
        id = {categories[selectedIndex]?._id}
        showModal = {modal}
        onHide = {showModal}
        onSubmit = {onSubmit}
      />
		</div>
	)
}

export default Category;