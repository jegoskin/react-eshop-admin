import React, { useEffect, useState } from 'react';
import postApi from '../../lib/api/post.api';
import { Card, Button, Table, Input } from 'antd';
import AddPostModal from './add-post-modal';
import style from '../../styles/table.module.css';
import { PlusCircleOutlined } from '@ant-design/icons';

function Post() {
  const [ posts, setPosts ] = useState([]);
  const [ selectedIndex, setSelectedIndex ] = useState(undefined);
  const [ modal, setModal ] = useState(false);

  const [ paginationTotal, setPaginationTotal ] = useState(0);
  const [ paginationCurrent, setPaginationCurrent ] = useState(1);
  const [ paginationPageSize, setPaginationPageSize ] = useState(10);

  const columns = [
    {
      title: 'Title',
      key: 'title',
      dataIndex: 'title',
      sorter: true,
      filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => {
        const input = React.createRef();
        setImmediate(() => {
          if (input.current) {
            input.current.focus();
          }
        });
        return (
          <div className={style.searchForm}>
            <Input
              ref={input}
              placeholder="Search..."
              value={selectedKeys[0]}
              onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
              onPressEnter={confirm}
            />
            <Button type="primary" onClick={confirm}>Search</Button>
            <Button onClick={clearFilters}>Reset</Button>
          </div>
        )
      }
    },
    {
      render: (_, row, index) => {
        return <>
          <Button style={{marginRight: '.3rem'}} type="primary" size="small" onClick={() => showModal(index)}>Edit</Button>
          <Button size="small" onClick={() => remove(row._id)}>Delete</Button>
        </>
      },
      width: '15%'
    }
  ]

  useEffect(() => {
    refresh();
  }, []);

  async function refresh(pagination, filter, sorter) {
		if (Object.keys(sorter || {}).length < 3) {
			sorter = { field: "date", order: "descend" }
    }

		let pageIndex = 0;
		if (pagination?.current !== undefined) {
			pageIndex = pagination.current - 1;
    }
    const pageSize = pagination?.pageSize || 10;

    try {
      const postData = await postApi.get(pageIndex, pageSize, filter, sorter);
      setPosts(postData.data);
      setPaginationTotal(postData.pagination.count);
      setPaginationCurrent(postData.pagination.pageIndex + 1);
      setPaginationPageSize(postData.pagination.pageSize);
    } catch(err) {
      console.error(err);
    }
  }

  const showModal = (index) => {
    setModal(!modal);
    setSelectedIndex(index);
  }

  async function onSubmit(newTitle, newDescription, id) {
    try {
      if (id) {
        await postApi.put(newTitle, newDescription, id);
      } else {
        await postApi.post(newTitle, newDescription);
      }
      showModal()
      refresh();
    } catch(err) {
      console.error(err);
    }
  }

  async function remove(id) {
    try {
      await postApi.remove(id);
      refresh();
    } catch(err) {
      console.error(err);
    }
  }

	return (
		<div>
      <Card
        title={
          <div style={{display: 'flex', justifyContent: 'space-between', alignItems: 'center'}}>
            <h2 style={{margin: '0'}}>Post</h2><PlusCircleOutlined style={{ fontSize: '1.5rem', color: '#40a9ff' }} onClick={() => showModal()} />
          </div>
        }
      >
        <Table
          columns={columns}
          dataSource={posts}
          rowKey={row => row._id}
          onChange={refresh}
          pagination = {{
            current: paginationCurrent,
            pageSize: paginationPageSize,
            pageSizeOptions: [ '10', '20', '50' ],
            showSizeChanger: true,
            total: paginationTotal
          }}
        />
      </Card>

      <AddPostModal
        title = {posts[selectedIndex]?.title}
        description = {posts[selectedIndex]?.description}
        id = {posts[selectedIndex]?._id}
        showModal = {modal}
        onHide = {showModal}
        onSubmit = {onSubmit}
      />
		</div>
	)
}

export default Post;