import React, { useState, useEffect } from 'react';
import { Modal, Input } from 'antd';

const AddPostModal = ({showModal, title, description, id, onHide, onSubmit}) => {
  const [ newTitle, setNewTitle ] = useState(title);
  const [ newDescription, setNewDescription ] = useState(description);

  useEffect(() => {
    if (showModal) {
      setNewTitle(title);
      setNewDescription(description);
    }
  }, [ showModal, title, description ]);

  async function onOk() {
    await onSubmit(newTitle, newDescription, id)
    setNewTitle('')
    setNewDescription('')
  }

  async function onCancel() {
    setNewTitle('')
    setNewDescription('')
    await onHide()
  }

  return (
    <div>
      <Modal
        title={`${id? 'Edit' : 'Add'} post`} 
        onOk={() => onOk()}
        onCancel={() => onCancel()}
        visible={showModal}
      >
        <Input 
          placeholder="Title"
          value={newTitle}
          onChange={e => setNewTitle(e.target.value)}
          style={{marginBottom: '1rem'}}
        />
        <Input.TextArea
          placeholder="Description"
          value={newDescription}
          onChange={e => setNewDescription(e.target.value)}
          autoSize={{ minRows: 5 }}
        />
      </Modal>
    </div>
  )
}

export default AddPostModal;