import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import Root from './pages/root/root';
import { SWRConfig } from 'swr';
import fetcher from './lib/fetcher';
import './style.css';
import { AuthProvider } from './lib/context-api/auth-context';

const App = () => (
	<SWRConfig value={{
		fetcher: fetcher,
	}}>
	<AuthProvider>
		<BrowserRouter>
			<Root/>
		</BrowserRouter>
	</AuthProvider>
	</SWRConfig>
)

ReactDOM.render(
	<App />
	, document.getElementById('root'));