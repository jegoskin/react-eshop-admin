import { useState } from "react";

export default function useFilter() {
  const [ pageIndex, setPageIndex ] = useState(0);
  const [ pageSize, setPageSize ] = useState(10);
  const [ filter, setFilter ] = useState({});
  const [ sorter, setSorter ] = useState({ field: undefined, order: undefined });

  const filterFiled = Object.keys(filter)[0];
  const filterString = Object.values(filter)[0];

  const filterQuery = `
  pageIndex=${pageIndex}
  &pageSize=${pageSize}
  &sorterField=${sorter.field}
  &sorterOrder=${sorter.order}
  &filterField=${filterFiled}
  &filterString=${filterString}`

  return [ pageIndex, pageSize, filterFiled, filterString, sorter, filterQuery ]
}