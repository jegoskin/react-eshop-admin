import * as config from '../../config';

class CategoryApi {
	async get() {
		const response = await fetch(`${config.API}/category`, {
			method: 'GET', 
			credentials: 'include',
			headers: {
				'Content-Type': 'application/json'
			}
		});
		const json = await response.json();
		return json;
	}

	async post(name) {
		await fetch(`${config.API}/category`, {
			method: 'POST', 
			credentials: 'include',
			body: JSON.stringify({
				name
			}),
			headers: {
				'Content-Type': 'application/json'
			}
		});
	}

	async put(name, id) {
		await fetch(`${config.API}/category/${id}`, {
			method: 'PUT', 
			credentials: 'include',
			body: JSON.stringify({
				name
			}),
			headers: {
				'Content-Type': 'application/json'
			}
		});
	}

	async remove(id) {
		await fetch(`${config.API}/category/${id}`, {
			method: 'DELETE', 
			credentials: 'include',
			headers: {
				'Content-Type': 'application/json'
			}
		});
	}
}

export default new CategoryApi();