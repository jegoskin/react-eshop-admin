import * as config from '../../config';

class AuthApi {
	async login(email, password) {
		const response = await fetch(`${config.API}/user/login`, {
				method: 'POST', 
				credentials: 'include',
				body: JSON.stringify({
					email,
					password
				}),
				headers: {
					'Content-Type': 'application/json'
				}
			});

			const json = await response.json();
			return json;
	}

	async logout() {
		await fetch(`${config.API}/user/logout`, {
				method: 'POST', 
				credentials: 'include',
				headers: {
					'Content-Type': 'application/json'
				}
			});
	}

	async refresh() {
		const response = await fetch(`${config.API}/user/refresh`, {
				method: 'POST', 
				credentials: 'include',
				headers: {
					'Content-Type': 'application/json'
				}
			});

			const json = await response.json();
			return json;
	}
}

export default new AuthApi();