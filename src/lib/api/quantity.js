import * as config from '../../config';

class QuantityApi {
	async getById(productId) {
		const response = await fetch(`${config.API}/quantity/${productId}`, {
			method: 'GET', 
			credentials: 'include',
			headers: {
				'Content-Type': 'application/json'
			}
    });
    const json = await response.json();
		return json;
	}
}

export default new QuantityApi();