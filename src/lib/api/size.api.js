import * as config from '../../config';

class SizeApi {
	async get() {
		const response = await fetch(`${config.API}/size`, {
			method: 'GET', 
			credentials: 'include',
			headers: {
				'Content-Type': 'application/json'
			}
		});
		const json = await response.json();
		return json;
	}

	async post(name) {
		await fetch(`${config.API}/size`, {
			method: 'POST', 
			credentials: 'include',
			body: JSON.stringify({
				name
			}),
			headers: {
				'Content-Type': 'application/json'
			}
		});
	}

	async put(name, id) {
		await fetch(`${config.API}/size/${id}`, {
			method: 'PUT', 
			credentials: 'include',
			body: JSON.stringify({
				name
			}),
			headers: {
				'Content-Type': 'application/json'
			}
		});
	}

	async remove(id) {
		await fetch(`${config.API}/size/${id}`, {
			method: 'DELETE', 
			credentials: 'include',
			headers: {
				'Content-Type': 'application/json'
			}
		});
	}
}

export default new SizeApi();