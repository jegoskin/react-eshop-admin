import * as config from '../../config';

class InvoiceApi {
	async get() {
		const response = await fetch(`${config.API}/invoice`, {
			method: 'GET', 
			credentials: 'include',
			headers: {
				'Content-Type': 'application/json'
			}
		});
		const json = await response.json();
		return json;
	}
}

export default new InvoiceApi();