import * as config from '../../config';

class SettingsApi {
	async get() {
		const response = await fetch(`${config.API}/settings`, {
			method: 'GET', 
			credentials: 'include',
			headers: {
				'Content-Type': 'application/json'
			}
		});
		const json = await response.json();
		return json;
	}
}

export default new SettingsApi();