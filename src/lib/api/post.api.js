import * as config from '../../config';

class PostApi {
	async get(pageIndex, pageSize, filter = {}, sorter) {
		const filterFiled = Object.keys(filter)[0];
		const filterString = Object.values(filter)[0];

		const response = await fetch(`${config.API}/post?
			pageIndex=${pageIndex}
			&pageSize=${pageSize}
			&sorterField=${sorter.field}
			&sorterOrder=${sorter.order}
			&filterField=${filterFiled}
			&filterString=${filterString && filterString[0]}
		`, {
			method: 'GET', 
			credentials: 'include',
			headers: {
				'Content-Type': 'application/json'
			}
		});
		const json = await response.json();
		return json;
	}

	async post(title, description) {
		await fetch(`${config.API}/post`, {
			method: 'POST', 
			credentials: 'include',
			body: JSON.stringify({
				title,
				description
			}),
			headers: {
				'Content-Type': 'application/json'
			}
		});
	}

	async put(title, description, id) {
		await fetch(`${config.API}/post/${id}`, {
			method: 'PUT', 
			credentials: 'include',
			body: JSON.stringify({
				title,
				description
			}),
			headers: {
				'Content-Type': 'application/json'
			}
		});
	}

	async remove(id) {
		await fetch(`${config.API}/post/${id}`, {
			method: 'DELETE', 
			credentials: 'include',
			headers: {
				'Content-Type': 'application/json'
			}
		});
	}
}

export default new PostApi();