import * as config from '../../config';

class RoleApi {
	async get() {
		const response = await fetch(`${config.API}/role`, {
			method: 'GET', 
			credentials: 'include',
			headers: {
				'Content-Type': 'application/json'
			}
		});
		const json = await response.json();
		return json;
	}
}

export default new RoleApi();