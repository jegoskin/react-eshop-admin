import * as config from '../../config';

class OrderApi {
	async get(pageIndex, pageSize, filter = {}, sorter) {
		const filterFiled = Object.keys(filter)[0];
		const filterString = Object.values(filter)[0];

		const response = await fetch(`${config.API}/order?
			pageIndex=${pageIndex}
			&pageSize=${pageSize}
			&sorterField=${sorter.field}
			&sorterOrder=${sorter.order}
			&filterField=${filterFiled}
			&filterString=${filterString && filterString[0]}
		`, {
			method: 'GET', 
			credentials: 'include',
			headers: {
				'Content-Type': 'application/json'
			}
		});
		const json = await response.json();
		return json;
	}

	async post(data) {
		await fetch(`${config.API}/order`, {
			method: 'POST',
			credentials: 'include',
			body: JSON.stringify({ ...data }),
			headers: {
				'Content-Type': 'application/json'
			}
		});
	}

	async put(name, id) {
		await fetch(`${config.API}/order/${id}`, {
			method: 'PUT',
			credentials: 'include',
			body: JSON.stringify({
				name
			}),
			headers: {
				'Content-Type': 'application/json'
			}
		});
	}

	async remove(id) {
		await fetch(`${config.API}/order/${id}`, {
			method: 'DELETE', 
			credentials: 'include',
			headers: {
				'Content-Type': 'application/json'
			}
		});
	}
}

export default new OrderApi();