import * as config from '../../config';

class ProductApi {
	async get(pageIndex, pageSize, filter = {}, sorter) {
		const filterFiled = Object.keys(filter)[0];
		const filterString = Object.values(filter)[0];

		const response = await fetch(`${config.API}/product?
			pageIndex=${pageIndex}
			&pageSize=${pageSize}
			&sorterField=${sorter.field}
			&sorterOrder=${sorter.order}
			&filterField=${filterFiled}
			&filterString=${filterString && filterString[0]}
		`, {
			method: 'GET',
			credentials: 'include',
			headers: {
				'Content-Type': 'application/json'
			}
		});
		const json = await response.json();
		return json;
	}

	async post(newName, newBrandId, newPrice, newCategoryId, newSize) {
		await fetch(`${config.API}/product`, {
			method: 'POST', 
			credentials: 'include',
			body: JSON.stringify({
				name: newName, 
				brandId: newBrandId, 
				categoryId: newCategoryId,
				price: newPrice,
				size: newSize
			}),
			headers: {
				'Content-Type': 'application/json'
			}
		});
	}

	async put(newName, newBrandId, newPrice, newCategoryId, newSize, id) {
		await fetch(`${config.API}/product/${id}`, {
			method: 'PUT', 
			credentials: 'include',
			body: JSON.stringify({
				name: newName, 
				brandId: newBrandId, 
				price: newPrice,
				categoryId: newCategoryId,
				size: newSize
			}),
			headers: {
				'Content-Type': 'application/json'
			}
		});
	}

	async remove(id) {
		await fetch(`${config.API}/product/${id}`, {
			method: 'DELETE', 
			credentials: 'include',
			headers: {
				'Content-Type': 'application/json'
			}
		});
	}
}

export default new ProductApi();