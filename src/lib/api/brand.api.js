import * as config from '../../config';

class BrandApi {
	async get() {
		const response = await fetch(`${config.API}/brand`, {
			method: 'GET', 
			credentials: 'include',
			headers: {
				'Content-Type': 'application/json'
			}
		});
		const json = await response.json();
		return json;
	}

	async post(name) {
		await fetch(`${config.API}/brand`, {
			method: 'POST', 
			credentials: 'include',
			body: JSON.stringify({
				name
			}),
			headers: {
				'Content-Type': 'application/json'
			}
		});
	}

	async put(name, id) {
		await fetch(`${config.API}/brand/${id}`, {
			method: 'PUT', 
			credentials: 'include',
			body: JSON.stringify({
				name
			}),
			headers: {
				'Content-Type': 'application/json'
			}
		});
	}

	async remove(id) {
		await fetch(`${config.API}/brand/${id}`, {
			method: 'DELETE', 
			credentials: 'include',
			headers: {
				'Content-Type': 'application/json'
			}
		});
	}
}

export default new BrandApi();