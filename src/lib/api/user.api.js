import { API } from '../../config';

class UserApi {
	async get(pageIndex, pageSize, filter = {}, sorter) {
		const filterFiled = Object.keys(filter)[0];
		const filterString = Object.values(filter)[0];

		const response = await fetch(`${API}/user?
			pageIndex=${pageIndex}
			&pageSize=${pageSize}
			&sorterField=${sorter.field}
			&sorterOrder=${sorter.order}
			&filterField=${filterFiled}
			&filterString=${filterString && filterString[0]}
		`, {
			method: 'GET',
			credentials: 'include',
			headers: {
				'Content-Type': 'application/json'
			}
		});
		const json = await response.json();
		return json;
	}

	async post(name, lastName, email, role) {
		await fetch(`${API}/user`, {
			method: 'POST', 
			credentials: 'include',
			body: JSON.stringify({
				name,
				lastName,
				email,
				role
			}),
			headers: {
				'Content-Type': 'application/json'
			}
		});
	}

	async postPassword(data) {
		await fetch(`${API}/user/password/${data._id}`, {
			method: 'POST', 
			credentials: 'include',
			body: JSON.stringify({ password: data.password }),
			headers: {
				'Content-Type': 'application/json'
			}
		});
	}

	async put(data) {
		await fetch(`${API}/user/${data._id}`, {
			method: 'PUT', 
			credentials: 'include',
			body: JSON.stringify({
				...data
			}),
			headers: {
				'Content-Type': 'application/json'
			}
		});
	}

	async remove(id) {
		await fetch(`${API}/user/${id}`, {
			method: 'DELETE', 
			credentials: 'include',
			headers: {
				'Content-Type': 'application/json'
			}
		});
	}
}

export default new UserApi();