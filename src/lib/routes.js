const routes = {
  LOGIN: 'login',
  POST: 'post',
  USER: 'user',
  PRODUCT: 'product',
  ORDER: 'order',
  SETTINGS: 'settings',
  ENUM: 'enum',
  BRAND: 'brand',
  SIZE: 'size',
  CATEGORY: 'category',
} 

export default routes;