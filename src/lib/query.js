class Query {
	static stringify(params) {
		if (!params) {
			return '';
		}
		const query = Object.entries(params)
			.filter(([ key, value ]) => !!value)
			.map(([ key, value ]) => `${key}=${value}`)
			.join('&')
		return '?' + query;
	}
}

export default Query;