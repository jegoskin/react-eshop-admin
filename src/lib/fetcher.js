import Query from "./query";
import { END_POINT } from '../config';

export default async function fetcher(url, { method = 'GET', params, body, endPoint = END_POINT } = {}) {
	const uriQuery = params ? Query.stringify(params) : '';
	const finalUrl = endPoint + url + uriQuery;
	const headers = {};

	if (!(body instanceof FormData)) {
		headers["content-type"] = "application/json";
	}

	const response = await fetch(finalUrl, {
		method,
		credentials: 'include',
		headers,
		body: body ? JSON.stringify(body) : undefined,
	})

  let content = null;

  try {
    content = await response.json();
  } catch { }

	return content;
}