import React, { useState, createContext } from 'react';
import AuthApi from '../api/auth.api';
export const AuthContext = createContext();

export const AuthProvider = props => {
	const [ userInfo, setUserInfo ] = useState({});
	const [ isLogged, setIsLogged ] = useState(false);

	async function login(emailLogin, passwordLogin) {
		const userData = await AuthApi.login(emailLogin, passwordLogin);
		setUserInfo({ ...userData });
		setIsLogged(true);
	}

	async function logout() {
		await AuthApi.logout()
	}

	async function refresh() {
		const userData = await AuthApi.refresh();
		setUserInfo({ ...userData });
		setIsLogged(true);
	}

	return (
		<AuthContext.Provider value={{login, logout, refresh, userInfo, isLogged }}>
			{ props.children }
		</AuthContext.Provider>
	)
};